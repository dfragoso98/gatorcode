import React, { useState, useEffect } from "react";
import "./styles/challenge.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Axios from "axios";
import { Row, Col, Accordion, Button, Card, Container } from "react-bootstrap";
import Ide from "../components/ide";
import { useParams } from "react-router-dom";

function Challenge(props) {
  const [userdata, setUserdata] = useState(props.data);
  const [challengeTitle, setChallengeTitle] = useState(useParams().title);
  const [challengeData, setChallengeData] = useState({});

  //states for challenge
  const [checkingAnswer, setCheckingAnswer] = useState(true);
  const [correct, setCorrect] = useState(false);
  const [alreadyComplete, setAlreadyComplete] = useState(false);
  const [incorrect, setIncorrect] = useState(false);
  const [invalidSolution, setInvalidSolution] = useState(false);
  const [desc, setDesc] = useState("");
  const [inputType, setInputType] = useState("");

  var userPoints = 0;
  var userLevel = 0;
  var completedChallenges = {};

  const getData = () => {
    return Axios({
      method: "post",
      url: "http://localhost:5000/getchallenge",
      data: {
        title: challengeTitle
      }
    });
  };

  const completeChallenge = async () => {
    const dataobj = await userdata;

    //console.log(document.getElementById("input").value);

    userPoints = dataobj.data.points;
    userLevel = dataobj.data.level;

    completedChallenges = dataobj.data.challengesCompleted;

    const complete = dataobj.data.challengesCompleted[challengeTitle];

    const correctOutput = challengeData.correctOutput;
    const userOutput = document.getElementById("result").value;

    //check for correct output
    if (correctOutput === userOutput) {
      //check for solution keys
      let keys = [];
      switch (localStorage.getItem("lang")) {
        case "java":
          keys = challengeData.keysjava;
          break;
        case "cpp":
          keys = challengeData.keyscpp;
          break;
        case "c":
          keys = challengeData.keysc;
          break;
        case "python":
          keys = challengeData.keyspython;
          break;
      }
      const code = localStorage.getItem("code");
      let validSolution = true;

      keys.map((i, j) => {
        if (!code.includes(keys[j])) {
          validSolution = false;
        }
      });

      if (validSolution) {
        //check for completion status
        if (complete === 0) {
          //add points
          setCheckingAnswer(false);
          setCorrect(true);
          setIncorrect(false);
          //alert("CORRECT!");
          switch (challengeData.difficulty) {
            case "Easy":
              userPoints += 5;
              break;
            case "Medium":
              userPoints += 10;
              break;
            case "Hard":
              userPoints += 15;
              break;
          }

          //mark as complete
          completedChallenges[challengeTitle] = 1;
          //console.log(completedChallenges);

          if (userPoints >= userLevel * 5) {
            userPoints = userPoints - userLevel * 5;
            userLevel += 1;
            addPoints();
          } else {
            addPoints();
          }
          window.location.reload();
        } else {
          setCheckingAnswer(false);
          setAlreadyComplete(true);
          setCorrect(true);
          setIncorrect(false);
        }
      } else {
        setCheckingAnswer(false);
        setIncorrect(true);
        setCorrect(false);

        //alert("INVALID SOLUTION!");
      }
    } else {
      //challenge attempted
      setCheckingAnswer(false);
      setIncorrect(true);
      setCorrect(false);

      //alert("INCORRECT!");
      if (complete !== 2) {
        completedChallenges[challengeTitle] = 2;
        addPoints();
      }
    }
  };

  const addPoints = async () => {
    const dataobj = await userdata;

    Axios({
      method: "PUT",
      data: {
        username: dataobj.data.username,
        points: userPoints,
        level: userLevel,
        challengesCompleted: completedChallenges
      },
      withCredentials: true,
      url: "http://localhost:5000/addpoints"
    });
  };

  const setData = async () => {
    const data = await getData();
    const dataobj = await userdata;
    userPoints = dataobj.data.points;
    userLevel = dataobj.data.level;
    await setChallengeData(data.data);
    //await setInputType(data.data.inputType);
    setCodeTemplate(data.data.inputType);
  };

  //set input value to specified value in challengeData depending on language
  const setInput = () => {
    let input = "";

    switch (localStorage.getItem("lang")) {
      case "java":
        input = challengeData.inputjava;
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
      case "cpp":
        input = challengeData.inputcpp;
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
      case "c":
        input = challengeData.inputc;
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
      case "python":
        input = challengeData.inputpython;
        console.log(input);
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
    }
  };

  const setCodeTemplate = inputTy => {
    const input = document.getElementById("input").value;

    const funcName = `${challengeTitle.toLowerCase().replaceAll(" ", "_")}`;

    console.log(inputTy);

    //!JAVA NOT CURRENTLY WORKING

    const javaTemplate = `public class myClass {

        public static void main (String[] args) {

            for (String s: args) {

                System.out.println(s);

            }

            ${funcName};

        }

    }
    
    public void ${funcName}() {

      //YOUR SOLUTION GOES HERE

    }`;

    //!C NOT CURRENTLY WORKING

    var cTemplate = ``;

    if (inputTy == "Array") {
      cTemplate = `#include <stdio.h>  
#include <unistd.h>  

void reverse_string(char *argus[]){

  //printf(argus[0])

  //for (int i = 0; i < 4; ++i)
  // {
  //  printf(argus[i]);
  //}
    

}
  
int main(int argc, char *argv[])  
{ 
    int opt; 
              
    const char *arguments[5];
  
    for(; optind < argc; optind++){      
        arguments[optind] = argv[optind];
        // printf("extra arguments: %s\n", argv[optind]);  
    } 

    reverse_string(arguments);
    
    return 0; 
} 
      `;
    } else if (
      inputTy == "String" ||
      inputTy == "Number" ||
      inputTy == "Object"
    ) {
      cTemplate = `#include <iostream>
#include <string>
      
void ${funcName}(std::string argument){
  //YOUR CODE GOES HERE
         
}
      
int main(int argc, char* argv[]){
  //DO NOT CHANGE
  //Parses input and calls your function with the input as a param
  std::string argument = std::string(argv[1]);
  ${funcName}(argument); //your function call
  return 0;
}`;
    }

    var cppTemplate = ``;

    if (inputTy == "Array") {
      cppTemplate = `#include <iostream>
#include <string>
#include <vector>
      
  
std::vector<std::string> parse_input(std::string args) {
  /*
  !!!DO NOT CHANGE!!!
  This function parses the input arguments and returns a vector<string> of arguments
  */
  
  std::vector<std::string> rect_vect;
  std::string s = args;
  std::string delimiter = ",";
    
  size_t pos = 0;
  std::string token;
  while ((pos = s.find(delimiter)) != std::string::npos) {
    token = s.substr(0, pos);
    s.erase(0, pos + delimiter.length());
    rect_vect.push_back(token);
  }
      
  return rect_vect;
  
}
      
void ${funcName}(std::vector<std::string> input) {
  /*
  YOUR SOLUTION GOES HERE
  Use '[]' to access elements in input
  i.e. input[0] will access the first element in the 'input' vector
  */
}
    
int main(int argc, char** argv) { 
  //stores the argumment value(s) from the 'Input' box below
  std::string input = argv[1];
    
  //passes the parsed input vector into your function as a param
  ${funcName}(parse_input(input));
    
  return 0; 

}
  `;
    } else if (
      inputTy == "String" ||
      inputTy == "Number" ||
      inputTy == "Object"
    ) {
      cppTemplate = `#include <iostream>
#include <string>
      
void ${funcName}(std::string argument){
  //YOUR CODE GOES HERE
         
}
      
int main(int argc, char* argv[]){
  //DO NOT CHANGE
  //Parses input and calls your function with the input as a param
  std::string argument = std::string(argv[1]);
  ${funcName}(argument); //your function call
  return 0;
}`;
    }

    const pythonTemplate = `import sys

#DO NOT CHANGE
#parse arguments and returns an array
def parse_input():
  return sys.argv[1].split(',')
    
def ${funcName}(parsed_input): 
  #YOUR SOLUTION GOES HERE

#function call with input as param
${funcName}(parse_input())
`;

    switch (localStorage.getItem("lang")) {
      case "java":
        document.getElementById("code").value = javaTemplate;
        break;
      case "python":
        document.getElementById("code").value = pythonTemplate;
        break;
      case "cpp":
        document.getElementById("code").value = cppTemplate;
        break;
      case "c":
        document.getElementById("code").value = cTemplate;
        break;
    }
  };

  //parse input type
  const handleInputType = () => {};

  const hintLangHandler = () => {
    switch (localStorage.getItem("lang")) {
      case "java":
        return (
          <Accordion>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  Hint 1
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>{challengeData.hint1java}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="1">
                  Hint 2
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="1">
                <Card.Body>{challengeData.hint2java}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="3">
                  Hint 3
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="3">
                <Card.Body>{challengeData.hint3java}</Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        );
      case "python":
        return (
          <Accordion>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  Hint 1
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>{challengeData.hint1python}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="1">
                  Hint 2
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="1">
                <Card.Body>{challengeData.hint2python}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="3">
                  Hint 3
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="3">
                <Card.Body>{challengeData.hint3python}</Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        );
      case "c":
        return (
          <Accordion>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  Hint 1
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>{challengeData.hint1c}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="1">
                  Hint 2
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="1">
                <Card.Body>{challengeData.hint2c}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="3">
                  Hint 3
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="3">
                <Card.Body>{challengeData.hint3c}</Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        );
      case "cpp":
        return (
          <Accordion>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  Hint 1
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>{challengeData.hint1cpp}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="1">
                  Hint 2
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="1">
                <Card.Body>{challengeData.hint2cpp}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="3">
                  Hint 3
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="3">
                <Card.Body>{challengeData.hint3cpp}</Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        );
    }
  };

  const renderStatus = () => {
    if (correct) {
      return (
        <svg
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 130.2 130.2"
        >
          <circle
            class="path circle"
            fill="none"
            stroke="#73AF55"
            stroke-width="6"
            stroke-miterlimit="10"
            cx="65.1"
            cy="65.1"
            r="62.1"
          />
          <polyline
            class="path check"
            fill="none"
            stroke="#73AF55"
            stroke-width="6"
            stroke-linecap="round"
            stroke-miterlimit="10"
            points="100.2,40.2 51.5,88.8 29.8,67.5 "
          />
        </svg>
      );
    } else if (incorrect) {
      return (
        <svg
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 130.2 130.2"
        >
          <circle
            class="path circle"
            fill="none"
            stroke="#D06079"
            stroke-width="6"
            stroke-miterlimit="10"
            cx="65.1"
            cy="65.1"
            r="62.1"
          />
          <line
            class="path line"
            fill="none"
            stroke="#D06079"
            stroke-width="6"
            stroke-linecap="round"
            stroke-miterlimit="10"
            x1="34.4"
            y1="37.9"
            x2="95.8"
            y2="92.3"
          />
          <line
            class="path line"
            fill="none"
            stroke="#D06079"
            stroke-width="6"
            stroke-linecap="round"
            stroke-miterlimit="10"
            x1="95.8"
            y1="38"
            x2="34.4"
            y2="92.2"
          />
        </svg>
      );
    } else {
      return <div className="loader"></div>;
    }
  };

  // const setDescMethod = async () => {
  //   await challengeData;
  //   setDesc(challengeData.desc.replace(/\\n/g, "<br>\n"));
  // };

  const fillData = () => {
    //setDescMethod();
    return (
      <Card style={{ width: "35rem" }}>
        <Card.Body>
          <Card.Title id="challengeTitle">{challengeData.title}</Card.Title>

          <Card.Body className="mb-2 text-muted">
            <p>
              <b>Introduction:</b>
            </p>
            {challengeData.intro}
          </Card.Body>
          <Card.Body className="mb-2 text-muted">
            <p>
              <b>Description:</b>
            </p>
            {challengeData.desc}
            {challengeData.figure ? (
              <Card.Body className="mb-2 text-muted">
                <img id="figure" src={challengeData.figure} alt="figure" />
              </Card.Body>
            ) : null}
          </Card.Body>
          {challengeData.example1 ? (
            <Card.Body className="mb-2 text-muted">
              <p>
                <b>Example 1: </b>
              </p>
              {challengeData.example1}
            </Card.Body>
          ) : null}
          {challengeData.example2 ? (
            <Card.Body className="mb-2 text-muted">
              <p>
                <b>Example 2: </b>
              </p>
              {challengeData.example2}
            </Card.Body>
          ) : null}
          {challengeData.example3 ? (
            <Card.Body className="mb-2 text-muted">
              <p>
                <b>Example 3: </b>
              </p>
              {challengeData.example3}
            </Card.Body>
          ) : null}

          <div className="inputoutput">
            {challengeData.inputjava ? (
              <Card.Body className="mb-2 text-muted">
                <p>
                  <b>Input: </b>
                </p>
                {challengeData.inputpython}
              </Card.Body>
            ) : null}

            <Card.Body className="mb-2 text-muted">
              <p>
                <b>Expected Output: </b>
              </p>
              {challengeData.correctOutput}
            </Card.Body>
            {renderSetInputButton()}
          </div>
        </Card.Body>
        {hintLangHandler()}
      </Card>
    );
  };

  const renderSetInputButton = () => {
    if (challengeData.inputjava) {
      return <Button onClick={setInput}>Set Input</Button>;
    } else {
      return (
        <Button id="setInputButton" disabled onClick={setInput}>
          Set Input
        </Button>
      );
    }
  };

  useEffect(() => {
    setData();
  }, []);

  return (
    <Row>
      <div className="challengeInfo">
        <Col>
          {fillData()}
          <Container>
            <Button id="checkAnswer" onClick={() => completeChallenge()}>
              Check Answer
            </Button>
            {renderStatus()}
          </Container>
        </Col>
      </div>
      <div className="sandboxide">
        <Col>
          <Ide />
          <Row></Row>
        </Col>
      </div>
    </Row>
  );
}

export default Challenge;
