import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup"

import "./styles/sandbox.css";
import Ide from "../components/ide";
function SandboxLanding() {
  return (
    <div className="">
      <Card style={{ width: "18rem" }}>
        <Card.Body>
          <Card.Title>Existing Sandboxes</Card.Title>
          <ListGroup variant="flush">
            <ListGroup.Item>Sandbox 1</ListGroup.Item>
            <ListGroup.Item>Sandbox 2</ListGroup.Item>
            <ListGroup.Item>Sandbox 3</ListGroup.Item>
          </ListGroup>
        </Card.Body>
      </Card>

      <Card style={{ width: "18rem" }}>
        <Card.Body>
          <Card.Title>Create New Sandbox</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">
            Select Language
          </Card.Subtitle>
          <Card.Link href="#">Python</Card.Link>
          <Card.Link href="#">C</Card.Link>
          <Card.Link href="#">C++</Card.Link>
          <Card.Link href="#">Java</Card.Link>
        </Card.Body>
      </Card>
    </div>
  );
}

export default SandboxLanding;
