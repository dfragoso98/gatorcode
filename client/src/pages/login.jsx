import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

import Axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Form,
  Button,
  FormGroup,
  FormControl,
  ControlLabel,
  Container,
  Jumbotron
} from "react-bootstrap";
import mainLogo from "../media/mainLogo.png";
function Login() {
  const [loginUsername, setLoginUsername] = useState("");
  const [loginPassword, setLoginPassword] = useState("");

  const [data, setData] = useState({});
  const [loggedIn, setLoggedIn] = useState(false);
  const history = useHistory();

  const loginUser = () => {
    Axios({
      method: "POST",
      data: {
        username: loginUsername,
        password: loginPassword
      },
      withCredentials: true,
      url: "http://localhost:5000/login"
    })
      .then(localStorage.setItem("usl", "aaa"))
      .then(history.push("/"));
  };

  return (
    <React.StrictMode>
      <div class="container">
        <Jumbotron className="headerReg">
          <h1 id="titleh1">GatorCode</h1>
          <p>Code the Gator way.</p>
        </Jumbotron>
      </div>

      <div>
        <Container>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              placeholder="Username"
              onChange={e => setLoginUsername(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="Password"
              placeholder="Password"
              onChange={e => setLoginPassword(e.target.value)}
            />
          </Form.Group>

          <Button onClick={loginUser}>Submit</Button>
        </Container>
      </div>
      <div className="loginPic"></div>
    </React.StrictMode>
  );
}

export default Login;
