import React, { useState } from "react";
import "./App.css";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Axios from "axios";
import Homepage from "./pages/homepage";
import Sandbox from "./pages/sandbox";
import Navbar from "./components/navbar";
import Login from "./pages/login";
import Register from "./pages/register";
import Leaderboards from "./pages/leaderboards";
import Learn from "./pages/learn";
import Challenges from "./pages/challenges";
import Challenge from "./pages/challenge";
import Contact from "./pages/contact";
import Settings from "./pages/settings";

function App() {
  const getUser = () => {
    return Axios({
      method: "GET",
      withCredentials: true,
      url: "http://localhost:5000/getuser"
    });
  };

  const isLoggedin = async () => {
    const getuser = getUser();
    const userdata = await getuser;
    if (userdata.data && localStorage.getItem("usl") === "aaa") {
      return true;
    } else {
      return false;
    }
  };

  return (
    <Router>
      <div>
        <Navbar data={getUser()} />
        <Switch>
          <Route path="/" exact component={Homepage} />
          <Route
            path="/settings"
            exact
            render={props => <Settings {...props} data={getUser()} />}
          />

          <Route path="/contactus" exact component={Contact} />

          <Route
            path="/sandbox"
            exact
            render={props => <Sandbox {...props} data={getUser()} />}
          />
          <Route path="/login" exact component={Login} />
          <Route
            path="/challenges"
            exact
            render={props => <Challenges {...props} data={getUser()} />}
          />
          <Route path="/register" exact component={Register} />
          <Route path="/learn" exact component={Learn} />
          <Route path="/leaderboards" exact component={Leaderboards} />
          <Route
            path="/challenge/:title"
            exact
            render={props => <Challenge {...props} data={getUser()} />}
          />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
