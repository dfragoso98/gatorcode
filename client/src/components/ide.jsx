/*
----------NOTES----------
TODO: add windows support
*/

import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import "./styles/ide.css";

const axios = require("axios");
const port = 8011;

function Ide() {
  const [name, setName] = useState(
    localStorage.getItem("name") ? localStorage.getItem("name") : ""
  );
  const [result, setResult] = useState("Submit code to see result.");
  const [code, setCode] = useState(localStorage.getItem("selected"));
  const [input, setInput] = useState("");

  const lang = localStorage.getItem("lang");

  const onSubmitCode = async e => {
    e.preventDefault();

    //parse input

    const inputArray = input.split(",");

    let fileName = "";
    if (localStorage.getItem("selectedtitle")) {
      //if in sandbox
      fileName = localStorage.getItem("selectedtitle");
      //console.log(fileName);
    } else if (document.getElementById("challengeTitle")) {
      //if in challenges
      //!Does not work with Java

      //console.log(document.getElementById("challengeTitle"));
      // const fileBody = document
      //   .getElementById("challengeTitle")
      //   .value.split(" ")
      //   .join("");
      // const fileSuffix = localStorage.getItem("lang");
      // fileName = fileBody + fileSuffix;
      fileName = `gcode.${localStorage.getItem("lang")}`;
      //console.log(fileName);
    } else if (document.getElementById("lessonTitle")) {
      //if in lessons
      const fileBody = document
        .getElementById("lessonTitle")
        .split(" ")
        .join("");
      const fileSuffix = localStorage.getItem("lang");
      fileName = fileBody + fileSuffix;
      //console.log(fileName);
    }

    axios
      .post(`http://localhost:${port}/code/submit${lang}/`, {
        code: code,
        input: input,
        lang: lang,
        fileName: fileName
      })
      .then(res => {
        console.log(res);
        if (res.data.stderr) {
          setResult(res.data.stderr);
        } else if (res.data.shortMessage) {
          setResult(res.data.shortMessage);
        } else {
          setResult(res.data);
        }
      });
  };

  const onCodeChange = e => {
    localStorage.setItem("code", e.target.value);
    setCode(localStorage.getItem("code"));

    let name = "";
    let date = "";
    const codeData = {
      name: `${name}`,
      code: `${code}`,
      language: `${lang}`,
      saveDate: `${date}`
    };
  };

  const onInputChange = e => {
    setInput(e.target.value);
  };

  const codebox = document.getElementById("code");
  if (codebox) {
    codebox.addEventListener("keydown", function(e) {
      if (e.key == "Tab") {
        e.preventDefault();
        var start = this.selectionStart;
        var end = this.selectionEnd;

        // set textarea value to: text before caret + tab + text after caret
        this.value =
          this.value.substring(0, start) + "\t" + this.value.substring(end);

        // put caret at right position again
        this.selectionStart = this.selectionEnd = start + 1;
      }
    });
  }

  return (
    <div className="ide-full">
      <Row>
        <Col>
          <div className="idemain">
            {name == "" && document.getElementById("sandselect") ? (
              <React.Fragment>
                <div className="">
                  <textarea
                    type="text"
                    id="code"
                    placeholder="Please select a sandbox."
                    onChange={onCodeChange}
                    spellCheck="false"
                    rows="30"
                    cols="50"
                    autoFocus="true"
                    disabled="true"
                    autoCapitalize="false"
                    autoCorrect="false"
                    wrap="off"
                  >
                    {code}
                  </textarea>
                </div>
                <div className="">
                  <p className="lead d-block my-0">
                    Input (Separated by commas)
                  </p>
                  <textarea
                    type="text"
                    id="input"
                    placeholder="Please select a sandbox."
                    onChange={onInputChange}
                    disabled="true"
                  ></textarea>
                </div>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <div className="">
                  <div className="code-textarea-wrapper">
                    <textarea
                      type="text"
                      id="code"
                      placeholder="CODE"
                      onChange={onCodeChange}
                      spellCheck="false"
                      rows="10"
                      cols="40"
                      autoFocus="true"
                      value={code}
                    >
                      {code}
                    </textarea>
                  </div>
                </div>
                <div className="input">
                  <p className="lead d-block my-0">
                    Input (Separated by commas)
                  </p>
                  <textarea
                    type="text"
                    id="input"
                    placeholder="cat,5,[75,67,13,1]"
                    value={input}
                    onChange={onInputChange}
                  ></textarea>
                </div>
              </React.Fragment>
            )}

            <button
              id="submitbutton"
              className="btn btn-success"
              onClick={onSubmitCode}
            >
              Submit Code
            </button>
            <div className="row">
              <div className="output">
                <p className="lead d-block my-0">Output</p>
                <textarea
                  type="text"
                  id="result"
                  disabled={false}
                  value={result}
                ></textarea>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default Ide;
