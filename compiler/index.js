/*
COMPILER
to run enter "node index.js" in terminal
*/


const cors = require("cors");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

const { router: crouter } = require("./langs/c.js");
const { router: cpprouter } = require("./langs/cpp.js");
const { router: pyrouter } = require("./langs/python.js");
const { router: javarouter } = require("./langs/java.js");

const port = 8011;

app.use("/code", crouter);
app.use("/code", cpprouter);
app.use("/code", pyrouter);
app.use("/code", javarouter);

app.listen(port, () => {
  console.log(`Compiler listening at http://localhost:${port}`);
});
