const mongoose = require("mongoose");

const user = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  ufemail: { type: String, required: true },
  year: { type: String, required: true },
  class: { type: Array, required: true },
  major: { type: String, required: true },
  level: { type: Number, required: true },
  points: { type: Number, required: true },
  avatar: { type: Number, required: true },
  sandboxes: { type: Object, required: true },
  lessonsCompleted: { type: Object, required: true },
  challengesCompleted: { type: Object, required: true }
});

module.exports = mongoose.model("User", user);
