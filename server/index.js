/*
SERVER
for development, run "npm run dev" to start server
*/

//inlude required packages
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const passport = require("passport");
const passportLocal = require("passport-local").Strategy;
const cookieParser = require("cookie-parser");
const bcrypt = require("bcryptjs");
const session = require("express-session");
const nodemailer = require("nodemailer");

const User = require("./schemas/userSchema");
const Lesson = require("./schemas/lessonSchema");
const Challenge = require("./schemas/challengeSchema");

//create new express app
const app = express();

mongoose.connect(
  "mongodb+srv://testuser:test123@cluster0.6c2mx.mongodb.net/test?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  () => {
    console.log("Mongoose is connected.");
  }
);

//Middlewear
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(
  cors({
    origin: "http://localhost:3000", // <-- location of react app
    credentials: true
  })
);
app.use(
  session({
    secret: "secretcode",
    resave: true,
    saveUninitialized: true
  })
);
app.use(cookieParser("secretcode"));
app.use(passport.initialize());
app.use(passport.session());
require("./passportConfig")(passport);

//Routes
// app.use('/', require('./routes/index'))
// app.use('/users', require('./routes/users'))
app.post("/login", (req, res, next) => {
  passport.authenticate("local", (err, user, info) => {
    if (err) throw err;
    if (!user) res.send("No User Exists");
    else {
      req.logIn(user, err => {
        if (err) throw err;
        res.send("Successfully authenticated.");
        console.log(req.user);
      });
    }
  })(req, res, next);
});

app.post("/logout", (req, res, next) => {
  req.logout();
  req.session.destroy(function(err) {
    if (!err) {
      res
        .status(200)
        .clearCookie("connect.sid", { path: "/" })
        .json({ status: "Success" });
    } else {
      console.log(err);
    }
  });
});

app.put("/newsandbox", (req, res, next) => {
  User.findOne({ username: req.body.username }, async (err, doc) => {
    if (doc) {
      const updatedSandboxes = req.body.sandboxes;
      doc.sandboxes = updatedSandboxes;
      doc.save();
    }
  });
});

app.put("/deletesandbox", (req, res, next) => {
  User.findOne({ username: req.body.username }, async (err, doc) => {
    if (doc) {
      const updatedSandboxes = req.body.sandboxes;
      doc.sandboxes = updatedSandboxes;
      doc.save();
    }
  });
});

app.put("/updatesandbox", (req, res, next) => {
  User.findOne({ username: req.body.username }, async (err, doc) => {
    if (doc) {
      const updatedSandboxes = req.body.sandboxes;
      doc.sandboxes = updatedSandboxes;
      doc.save();
    }
  });
});

app.put("/addpoints", (req, res, next) => {
  console.log(req.body);

  User.findOne({ username: req.body.username }, async (err, doc) => {
    if (doc) {
      const updatedPoints = req.body.points;
      const updatedLevel = req.body.level;
      const updatedCompletedChallenges = req.body.challengesCompleted;
      doc.points = updatedPoints;
      doc.level = updatedLevel;
      doc.challengesCompleted = updatedCompletedChallenges;
      doc.save();
    }
  });
});

app.post("/register", (req, res) => {
  User.findOne({ username: req.body.username }, async (err, doc) => {
    if (err) throw err;
    if (doc) res.send("User Already Exists");
    if (!doc) {
      const hashedPassword = await bcrypt.hash(req.body.password, 10);
      const newUser = new User({
        username: req.body.username,
        password: hashedPassword,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        ufemail: req.body.ufemail,
        level: req.body.level,
        points: req.body.points,
        avatar: req.body.avatar,
        major: req.body.major,
        year: req.body.year,
        class: req.body.class,
        sandboxes: req.body.sandboxes,
        lessonsCompleted: req.body.lessonsCompleted,
        challengesCompleted: req.body.challengesCompleted
      });
      await newUser.save();
      res.send("User Created!");
    }
  });
});

app.post("/updateuser", (req, res) => {
  User.findOne({ username: req.body.username }, async (err, doc) => {
    if (err) throw err;
    if (doc) {
      const updatedUser = new User({
        username: req.body.username,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        ufemail: req.body.ufemail,
        avatar: req.body.avatar,
        major: req.body.major,
        year: req.body.year
      });
      await updatedUser.save();
      res.send("User Updated!");
    }
  });
});

app.post("/newlesson", (req, res) => {
  Lesson.findOne({ title: req.body.title }, async (err, doc) => {
    if (err) throw err;
    if (doc) res.send("Lesson Already Exists");
    if (!doc) {
      const newLesson = new Lesson({
        title: req.body.title,
        desc: req.body.desc
      });
      await newLesson.save();
      res.send("Lesson Created!");
    }
  });
});

app.post("/newchallenge", (req, res) => {
  Lesson.findOne({ title: req.body.title }, async (err, doc) => {
    if (err) throw err;
    if (doc) res.send("Challenge Already Exists");
    if (!doc) {
      const newChallenge = new Challenge({
        difficulty: req.body.difficulty,
        title: req.body.title,
        desc: req.body.desc,
        intro: req.body.intro,
        figure: req.body.figure,
        correctOutput: req.body.correctOutput,
        example1: req.body.example1,
        example2: req.body.example2,
        example3: req.body.example3,
        hint1java: req.body.hint1java,
        hint2java: req.body.hint2java,
        hint3java: req.body.hint3java,
        hint1python: req.body.hint1python,
        hint2python: req.body.hint2python,
        hint3python: req.body.hint3python,
        hint1cpp: req.body.hint1cpp,
        hint2cpp: req.body.hint2cpp,
        hint3cpp: req.body.hint3cpp,
        hint1c: req.body.hint1c,
        hint2c: req.body.hint2c,
        hint3c: req.body.hint3c,
        keysjava: req.body.keysjava,
        keyspython: req.body.keyspython,
        keyscpp: req.body.keyscpp,
        keysc: req.body.keysc,
        testsjava: req.body.testjava,
        testspython: req.body.testspython,
        testscpp: req.body.testscpp,
        testsc: req.body.testsc,
        inputjava: req.body.inputjava,
        inputpython: req.body.inputpython,
        inputcpp: req.body.inputcpp,
        inputc: req.body.inputc,
        inputType: req.body.inputType
      });
      await newChallenge.save();
      res.send("Challenge Created!");
    }
  });
});

//get all challenges
app.get("/getallchallenges", (req, res) => {
  Challenge.find({}, (err, challenges) => {
    if (err) console.log(err);

    const entries = Object.entries(challenges);

    //console.log(entries);

    var challengeMap = [];

    entries.map((i, j) => {
      const challVal = entries[j][1];

      //console.log(challVal);

      tempobj = {
        title: challVal.title,
        difficulty: challVal.difficulty,
        major: challVal.major
      };

      challengeMap.push(tempobj);
    });

    res.send(challengeMap);
  });
});

//get single challenge
app.post("/getchallenge", (req, res) => {
  const title = req.body.title;

  Challenge.findOne({ title: title }, (err, challenge) => {
    if (err) console.log(err);

    res.send(challenge);
  });
});

app.get("/getuser", (req, res) => {
  res.send(req.user); //this req.user stores the entire user that has been authenticated
});

app.get("/getallusers", (req, res) => {
  User.find({}, function(err, users) {
    const entries = Object.entries(users);

    //console.log(entries);

    var userMap = [];

    entries.map((i, j) => {
      //console.log(entries[j][1]);
      const studentInfo = entries[j][1];
      tempobj = {
        username: studentInfo.username,
        level: studentInfo.level,
        points: studentInfo.points,
        major: studentInfo.major,
        year: studentInfo.year
      };
      userMap.push(tempobj);
    });

    //console.log(usermap);

    res.send(userMap);
  });
});

//setup email SMTP
let transporter = nodemailer.createTransport({
  host: "smtp.gmail.com", //email provider
  port: 587,
  secure: false,
  auth: {
    user: "ufgatorcode@gmail.com", //email address
    pass: "G@T0R(0D3" //password
  }
});

// verify connection configuration
// transporter.verify((error, success) => {
//   if (error) {
//     console.log(error);
//   } else {
//     console.log("Server is ready to take our messages");
//   }
// });

//send message
app.post("/sendmessage", async (req, res, next) => {
  var name = req.body.name;
  var email = req.body.email;
  var message = req.body.message;
  var content = `name: ${name} \n email: ${email} \n message: ${message} `;
  var mail = {
    from: name,
    to: "ufgatorcode@gmail.com",
    subject: "GatorCode Contact Form",
    text: content
  };
  await transporter.sendMail(mail, (err, data) => {
    if (err) {
      res.json({
        status: "fail"
      });
    } else {
      res.json({
        status: "success"
      });
    }
  });
});

//define express server port
const PORT = process.env.PORT || 5000;

//activate express server
app.listen(PORT, () => {
  console.log(`Server listening at http://localhost:${PORT}`);
});
